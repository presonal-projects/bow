import * as React from 'react';
import 'react-native-gesture-handler';
import AppLoading from 'expo-app-loading';
import { StatusBar } from 'react-native';
import Navigation from './src/routes/Navigation';
import {
  useFonts,
  Montserrat_400Regular,
  Montserrat_400Regular_Italic,
  Montserrat_500Medium,
  Montserrat_500Medium_Italic,
  Montserrat_700Bold,
  Montserrat_700Bold_Italic,
  Montserrat_800ExtraBold,
  Montserrat_800ExtraBold_Italic,
} from '@expo-google-fonts/montserrat';
import { ConversionContextProvider } from './src/utils/ConversionContext';

const App = () => {
  let [fontsLoaded] = useFonts({
    Montserrat_400Regular,
    Montserrat_400Regular_Italic,
    Montserrat_500Medium,
    Montserrat_500Medium_Italic,
    Montserrat_700Bold,
    Montserrat_700Bold_Italic,
    Montserrat_800ExtraBold,
    Montserrat_800ExtraBold_Italic,
  });
  if (!fontsLoaded) {
    return <AppLoading />;
  } else {
    return (
      <ConversionContextProvider>
        <StatusBar barStyle='light-content' />
        <Navigation />
      </ConversionContextProvider>
    );
  }
};

export default App;
