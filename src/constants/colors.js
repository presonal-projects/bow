export default {
  colorWhite: '#FAFAFA',
  colorGrey: '#373737',
  colorRed: '#F22613',
  colorOrange: '#F26716',
  colorCircleTwo: '#F26716',
  colorCircleThree: '#F2B705',
  colorCircleFour: '#F2E205',
};
