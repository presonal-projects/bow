import React from 'react';
import {StyleSheet, View, Text, Button} from 'react-native';

const Informations = ({ navigation }) => {
    return(
        <View style={styles.sectionHeader}>
            <Text style={styles.sectionTitle}>Informations Page</Text>
            <Button
                title="About us... again"
                onPress={() => navigation.navigate('About us')}
            />
            <Button title="Go to Home" onPress={() => navigation.navigate('Home')} />
            <Button title="Go back" onPress={() => navigation.goBack()} />
        </View>
    )
}

const styles = StyleSheet.create({
    sectionContent: {
        flex: 1,
    },
    sectionNav: {
        flex: 1,
        alignItems: 'center',
        justifyContent: 'center',
    },
    sectionHeader: {
        flex: 10,
        alignItems: 'center',
    },
    sectionTitle: {
        textAlign: 'center',
        fontSize: 24,
        fontWeight: '700',
        color: '#414141',
    },
});

export default Informations;
