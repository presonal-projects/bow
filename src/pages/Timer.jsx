import React, { useState } from 'react';
import {
  View,
  StyleSheet,
  SafeAreaView,
  Text,
  TouchableOpacity,
  Dimensions,
} from 'react-native';
import { Entypo } from '@expo/vector-icons';
import { CountdownCircleTimer } from 'react-native-countdown-circle-timer';

import TimerButton from '../components/TimerButton';
import colors from '../constants/colors';

const styles = StyleSheet.create({
  container: {
    flex: 1,
  },
  timer: {
    flex: 2,
    justifyContent: 'center',
    alignItems: 'center',
  },
  chronometer: {
    fontFamily: 'Montserrat_500Medium',
    fontSize: 40,
  },
  actions: {
    flex: 1,
  },
  buttons: {
    flex: 2,
    justifyContent: 'space-evenly',
    alignItems: 'center',
    flexDirection: 'row',
  },
  input: {
    flex: 1,
    justifyContent: 'center',
    marginHorizontal: 50,
    borderColor: colors.colorCircleThree,
    borderWidth: 2,
    borderRadius: 5,
  },
  inputTouchable: {
    flex: 1,
    justifyContent: 'space-between',
    alignItems: 'center',
    flexDirection: 'row',
    width: '100%',
    paddingHorizontal: 20,
  },
  inputText: {
    color: colors.colorCircleThree,
    fontFamily: 'Montserrat_700Bold',
    fontSize: 20,
    letterSpacing: 1,
  },
  timerNumbers: {
    fontFamily: 'Montserrat_500Medium',
    fontSize: 30,
    color: colors.colorCircleThree,
  },
});

export default ({ navigation }) => {
  const [duration, setDuration] = useState(5);
  const [isPlaying, setIsPlaying] = useState(false);

  const { width } = Dimensions.get('window');

  const children = ({ remainingTime }) => {
    if (remainingTime === 0) {
      return <Text style={styles.timerNumbers}>Well done!</Text>;
    }

    let minutes = Math.floor(remainingTime / 60);
    let seconds = remainingTime % 60;

    if (minutes < 10) {
      minutes = `0${minutes}`;
    }
    if (seconds < 10) {
      seconds = `0${seconds}`;
    }

    return (
      <Text style={styles.timerNumbers}>
        {minutes} : {seconds}
      </Text>
    );
  };

  const startTimer = () => {
    if (!isPlaying) {
      setIsPlaying(true);
    } else {
      setIsPlaying(false);
    }
  };

  return (
    <SafeAreaView style={styles.container}>
      {/* Timer Block */}
      <View style={styles.timer}>
        <CountdownCircleTimer
          isPlaying={isPlaying}
          duration={duration}
          size={width - 100}
          colors={colors.colorCircleThree}
        >
          {children}
        </CountdownCircleTimer>
      </View>
      {/* Timer Actions */}
      <View style={styles.actions}>
        {/* Timer Duration */}
        <View style={styles.input}>
          <TouchableOpacity
            style={styles.inputTouchable}
            onPress={() => navigation.push('TimerRoller')}
          >
            <Text style={styles.inputText}>00:00</Text>
            <Entypo
              name='chevron-right'
              size={30}
              color={colors.colorCircleThree}
            />
          </TouchableOpacity>
        </View>
        {/* Timer Buttons */}
        <View style={styles.buttons}>
          <TimerButton title={'Start'} action={startTimer} />
          {/* <TimerButton title={'Reset'} action={resetTimer} /> */}
        </View>
      </View>
    </SafeAreaView>
  );
};
