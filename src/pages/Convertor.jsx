import React, { useState, useContext } from 'react';
import {
  StyleSheet,
  View,
  TouchableOpacity,
  Image,
  ActivityIndicator,
} from 'react-native';
import VirtualKeyboard from 'react-native-virtual-keyboard';
import ImgRevert from '../assets/img/icon-revert.png';
import CurrencyText from '../components/CurrencyText';
import Header from '../components/Header';
import { ConversionContext } from '../utils/ConversionContext';

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#FF8640',
  },
  currencyContainer: {
    flex: 1,
    position: 'relative',
  },
  numberContainer: {
    flex: 1,
    justifyContent: 'flex-start',
  },
  revert: {
    position: 'absolute',
    right: 20,
    top: '50%',
    zIndex: 99,
    transform: [{ translateY: -50 }],
    width: 70,
    height: 70,
    flex: 1,
    borderRadius: 100 / 2,
    elevation: 10,
    shadowOffset: { width: 0, height: 2 },
    shadowOpacity: 0.3,
    backgroundColor: '#FAFAFA',
    flexDirection: 'row',
    justifyContent: 'center',
    alignItems: 'center',
  },
  revertButton: {
    width: '100%',
    height: '100%',
    borderRadius: 100 / 2,
    flexDirection: 'row',
    justifyContent: 'center',
    alignItems: 'center',
  },
  revertIcon: {
    width: 30,
    height: 30,
  },
  loading: {
    flex: 1,
    display: 'flex',
    alignItems: 'flex-start',
    marginLeft: 40,
  },
});

export default ({ navigation }) => {
  // Use variables from context
  const {
    baseCurrency,
    quoteCurrency,
    swapCurrencies,
    rates,
    isLoading,
  } = useContext(ConversionContext);

  const conversionRate = rates[quoteCurrency];
  const [value, setValue] = useState('1');

  return (
    <View style={styles.container}>
      <View style={styles.currencyContainer}>
        {/* If isLoading is true display loading animation */}
        {isLoading ? (
          <ActivityIndicator
            color={'#fff'}
            size='large'
            style={styles.loading}
          />
        ) : (
          /*
           * Component for Base currency with :
           * - value
           * - open modal with variables pushed
           */
          <CurrencyText
            currency={baseCurrency}
            value={value}
            openModal={() =>
              navigation.push('CurrencyList', {
                title: 'Base Currency',
                isBaseCurrency: true,
              })
            }
          />
        )}

        {/* Swap currencies button */}
        <View style={styles.revert}>
          <TouchableOpacity
            style={styles.revertButton}
            onPress={() => swapCurrencies()}
          >
            <Image source={ImgRevert} style={styles.revertIcon} />
          </TouchableOpacity>
        </View>

        {/* If isLoading is true display loading animation */}
        {isLoading ? (
          <ActivityIndicator
            color={'#fff'}
            size='large'
            style={styles.loading}
          />
        ) : (
          /*
           * Component for Quote currency with :
           * - formule for conversion if value is set
           * - open modal with variables pushed
           */
          <CurrencyText
            currency={quoteCurrency}
            value={
              value && `${(parseFloat(value) * conversionRate).toFixed(2)}`
            }
            openModal={() =>
              navigation.push('CurrencyList', {
                title: 'Quote Currency',
                isBaseCurrency: false,
              })
            }
          />
        )}
      </View>

      {/* Virtual Keyboard */}
      <View style={styles.numberContainer}>
        <VirtualKeyboard
          color='#FAFAFA'
          pressMode='string'
          decimal={true}
          cellStyle={{ paddingVertical: 10 }}
          onPress={(val) => setValue(val)}
        />
      </View>
    </View>
  );
};
