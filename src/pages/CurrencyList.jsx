import React, { useContext } from 'react';
import { View, SafeAreaView, StyleSheet } from 'react-native';
import { Entypo } from '@expo/vector-icons';
import { FlatList } from 'react-native-gesture-handler';

import currencies from '../data/curriencies.json';
import colors from '../constants/colors';
import { CurrencyRow, RowSeparator } from '../components/CurrencyRow';
import { ConversionContext } from '../utils/ConversionContext';

const styles = StyleSheet.create({
  icon: {
    width: 30,
    height: 30,
    borderRadius: 20,
    alignItems: 'center',
    justifyContent: 'center',
    backgroundColor: colors.colorOrange,
  },
});

export default ({ navigation, route = {} }) => {
  // Use variables from context
  const {
    baseCurrency,
    quoteCurrency,
    setBaseCurrency,
    setQuoteCurrency,
  } = useContext(ConversionContext);

  // Use variables pushed through the navigation route
  const params = route.params || {};
  const { isBaseCurrency } = params;

  return (
    <SafeAreaView style={{ flex: 1 }}>
      <FlatList
        // Data imported from JSON
        data={currencies}
        // Loop to display each currencies
        renderItem={({ item }) => {
          // Display check icon on the right of currency active
          let selected = false;

          if (isBaseCurrency && item === baseCurrency) {
            selected = true;
          } else if (!isBaseCurrency && item === quoteCurrency) {
            selected = true;
          }

          return (
            // Component for one row
            <CurrencyRow
              // One currency name
              text={item}
              // Set currency choose and exit modal screen
              onPress={() => {
                if (isBaseCurrency) {
                  setBaseCurrency(item);
                } else {
                  setQuoteCurrency(item);
                }
                navigation.pop();
              }}
              // Actual currency show check icon on the right
              rightIcon={
                selected && (
                  <View style={styles.icon}>
                    <Entypo name='check' size={20} color={colors.colorWhite} />
                  </View>
                )
              }
            />
          );
        }}
        // Initialize one key for each items
        keyExtractor={(item) => item}
        // Separator between each items
        ItemSeparatorComponent={() => <RowSeparator />}
      />
    </SafeAreaView>
  );
};
