import React, { useState } from 'react';
import {
  Text,
  View,
  TextInput,
  TouchableWithoutFeedback,
  Keyboard,
  Alert,
  StyleSheet,
} from 'react-native';
import RandomButton from '../components/RandomButton';

const styles = StyleSheet.create({
  body: {
    backgroundColor: '#FAFAFA',
    resizeMode: 'cover',
    justifyContent: 'center',
    flex: 1,
    paddingHorizontal: 40,
  },
  textNumber: {
    fontSize: 40,
    textAlign: 'center',
    color: '#F22613',
    fontFamily: 'Montserrat_700Bold',
  },
  containerInput: {
    flexDirection: 'row',
    justifyContent: 'space-between',
    height: 60,
    marginTop: 40,
    marginBottom: 20,
  },
  textInput: {
    width: '48%',
    textAlign: 'center',
    borderWidth: 2,
    borderRadius: 10,
    borderColor: '#F22613',
  },
});

function Random() {
  const [prevNumber, setPrevNumber] = useState('');
  const [min, setMin] = useState('');
  const [max, setMax] = useState('');
  const [number, setNumber] = useState(Math.floor(Math.random() * 100) + 1);

  const getRandom = (min, max) =>
    Math.floor(Math.random() * (parseInt(max) - parseInt(min))) + parseInt(min);

  const incrementNumber = () => {
    return setNumber(number + 1);
  };

  const decrementNumber = () => {
    return setNumber(number - 1);
  };

  const randomNumber = () => {
    setMin('');
    setMax('');
  };

  const launchNumber = () => {
    if (min === '' || max === '') {
      Alert.alert(
        'Incomplet field',
        'Could you field min and max number please'
      );
    } else {
      setNumber(getRandom(min, max));

      if (number === prevNumber) {
        setNumber(getRandom(min, max));
        setPrevNumber(number);
      } else {
        setPrevNumber(number);
      }
    }
  };

  if (number < 0) {
    setNumber(0);
  }

  return (
    <TouchableWithoutFeedback
      onPress={() => {
        Keyboard.dismiss();
      }}
    >
      <View style={styles.body}>
        <View>
          <Text style={styles.textNumber}> {number} </Text>
        </View>
        <View style={styles.containerInput}>
          <TextInput
            keyboardType='number-pad'
            placeholder='Minimum'
            onChangeText={(min) => setMin(min)}
            value={min}
            style={styles.textInput}
          />
          <TextInput
            keyboardType='number-pad'
            placeholder='Maximum'
            onChangeText={(max) => setMax(max)}
            value={max}
            style={styles.textInput}
          />
        </View>
        <View>
          <RandomButton onPress={incrementNumber}>
            {' '}
            Increment number{' '}
          </RandomButton>
          <RandomButton onPress={decrementNumber}>
            {' '}
            Decrement number{' '}
          </RandomButton>
          <RandomButton onPress={randomNumber}>
            {' '}
            Reset Min and Max{' '}
          </RandomButton>
          <RandomButton onPress={launchNumber}> Start random </RandomButton>
        </View>
      </View>
    </TouchableWithoutFeedback>
  );
}

export default Random;
