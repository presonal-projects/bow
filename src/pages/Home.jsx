import React from 'react';
import { View, Text, ImageBackground, Image } from 'react-native';

import { globalStyles } from '../assets/css/style';
import HomeButton from '../components/HomeButton';
import ImgRandom from '../assets/img/icon-random.png';
import ImgCoins from '../assets/img/icon-coins.png';
import ImgCalc from '../assets/img/icon-timer.png';
import ImgCard from '../assets/img/icon-card.png';
import Header from '../components/Header';

const Home = ({ navigation }) => {
  return (
    <ImageBackground
      source={require('../assets/img/back.png')}
      style={globalStyles.homeContainer}
    >
      <View style={globalStyles.sectionHeader}>
        <Header
          openDrawer={() => navigation.openDrawer()}
          openSettings={() => navigation.navigate('settings')}
        />
        <Text style={globalStyles.homeTitle}>Bow</Text>
        <Text style={globalStyles.homeSubtitle}>
          Your new application multifunction
        </Text>
      </View>
      <View style={globalStyles.bodyHome}>
        <View style={globalStyles.row}>
          <HomeButton
            navigTo={() => navigation.navigate('random')}
            title={'Random Number'}
            icon={ImgRandom}
          />
          <HomeButton
            navigTo={() => navigation.navigate('timer')}
            title={'Timer'}
            icon={ImgCalc}
          />
        </View>
        <View style={globalStyles.row}>
          <HomeButton
            navigTo={() => navigation.navigate('convertor')}
            title={'Convertor'}
            icon={ImgCoins}
          />
          <HomeButton
            navigTo={() => navigation.navigate('wallet')}
            title={'Wallet'}
            icon={ImgCard}
          />
        </View>
      </View>
    </ImageBackground>
  );
};

export default Home;
