import React from 'react';
import { StyleSheet, Text, TouchableOpacity, View } from 'react-native';
import colors from '../constants/colors';

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: colors.colorCircleThree,
    display: 'flex',
    justifyContent: 'center',
  },
  timePicker: {
    backgroundColor: 'purple',
    flex: 10,
    display: 'flex',
    justifyContent: 'center',
    alignItems: 'center',
    flexDirection: 'row',
  },
  timeValidation: {
    backgroundColor: colors.colorCircleThree,
    justifyContent: 'center',
    alignItems: 'center',
    flex: 1,
  },
  text: {
    color: colors.colorWhite,
    height: 60,
    fontFamily: 'Montserrat_700Bold',
    fontSize: 40,
    textAlign: 'center',
  },
  minutes: {
    marginHorizontal: 20,
    height: 60 * 5,
    overflow: 'hidden',
  },
  separator: {},
  seconds: {
    marginHorizontal: 20,
    height: 60 * 5,
    overflow: 'hidden',
  },
  button: {
    flex: 1,
    width: '100%',
    justifyContent: 'center',
    alignItems: 'center',
  },
  buttonText: {
    color: colors.colorWhite,
    fontFamily: 'Montserrat_500Medium',
    fontSize: 25,
  },
});

export default () => {
  let time = [];

  for (let i = 0; i <= 60; i++) {
    time.push(i);
  }

  return (
    <View style={styles.container}>
      <View style={styles.timePicker}>
        <View style={styles.minutes}>
          {time.map((data) => (
            <Text style={styles.text} key={data.toString()}>
              {data}
            </Text>
          ))}
        </View>

        <View style={styles.separator}>
          <Text style={styles.text}>:</Text>
        </View>

        <View style={styles.seconds}>
          {time.map((data) => (
            <Text style={styles.text} key={data.toString()}>
              {data}
            </Text>
          ))}
        </View>
      </View>
      <View style={styles.timeValidation}>
        <TouchableOpacity
          style={styles.button}
          onPress={() => console.log('Confirm Timer TODO')}
        >
          <Text style={styles.buttonText}>Confirm Timer</Text>
        </TouchableOpacity>
      </View>
    </View>
  );
};
