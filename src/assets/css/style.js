import { StyleSheet } from 'react-native';
import colors from '../../constants/colors';

export const globalStyles = StyleSheet.create({
  homeContainer: {
    resizeMode: 'cover',
    flex: 1,
    paddingHorizontal: 20,
    position: 'relative',
  },
  container: {
    marginHorizontal: 40,
  },
  sectionHeader: {
    flex: 2,
    justifyContent: 'center',
  },
  homeTitle: {
    textAlign: 'center',
    fontStyle: 'italic',
    fontFamily: 'Montserrat_800ExtraBold_Italic',
    fontSize: 40,
    fontWeight: '700',
    color: colors.colorWhite,
  },
  homeSubtitle: {
    fontFamily: 'Montserrat_700Bold',
    textAlign: 'center',
    fontSize: 30,
    fontWeight: '600',
    color: colors.colorWhite,
  },
  bodyHome: {
    flex: 4,
    justifyContent: 'center',
    alignItems: 'center',
  },
  row: {
    flexDirection: 'row',
    justifyContent: 'space-around',
  },
});
