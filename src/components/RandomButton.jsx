import * as React from 'react';
import { StyleSheet, Text, View, TouchableHighlight } from 'react-native';

const ButtonRandom = props => {
    return(
        <View style={styles.container}>
            <TouchableHighlight onPress={props.onPress} style={styles.button}>
                <View>
                    <Text style={styles.text}>
                        {props.children}
                    </Text>
                </View>
            </TouchableHighlight>
        </View>
    );
};

const styles = StyleSheet.create({
    container: {
        marginVertical: 10,
    },
    button: {
        borderRadius: 10,
        overflow: 'hidden',
    },
    text: {
        textAlign: 'center',
        color: '#FAFAFA',
        backgroundColor: '#F22613',
        fontFamily: 'Montserrat_700Bold',
        fontSize: 20,
        borderRadius: 10,
        paddingVertical: 10,
        overflow: 'hidden',
    },
})

export default ButtonRandom;
