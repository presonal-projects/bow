import React from 'react';
import { TouchableOpacity, StyleSheet, View } from 'react-native';
import { Entypo } from '@expo/vector-icons';

const styles = StyleSheet.create({
  container: {
    display: 'flex',
    flexDirection: 'row',
    justifyContent: 'space-between',
  },
});

export default ({ openDrawer, openSettings }) => {
  return (
    <View style={styles.container}>
      <TouchableOpacity onPress={openDrawer}>
        <Entypo name='menu' size={35} color='white' />
      </TouchableOpacity>
      <TouchableOpacity onPress={openSettings}>
        <Entypo name='cog' size={30} color='white' />
      </TouchableOpacity>
    </View>
  );
};
