import React, { useEffect, useRef } from 'react';
import {
  View,
  TextInput,
  Animated,
  StyleSheet,
  Dimensions,
  Easing,
} from 'react-native';
import Svg, { Circle, G } from 'react-native-svg';
import colors from '../constants/colors';

const styles = StyleSheet.create({
  text: {
    fontFamily: 'Montserrat_500Medium',
    textAlign: 'center',
    alignItems: 'center',
  },
});

const { width } = Dimensions.get('window');

// Create new component for animate circle
const AnimatedCircle = Animated.createAnimatedComponent(Circle);
const AnimatedInput = Animated.createAnimatedComponent(TextInput);

export default ({
  time,
  radius = width / 2 - 50,
  strokeWidth = 15,
  color = colors.colorCircleThree,
}) => {
  // Create animated value using ref
  const animatedValue = useRef(new Animated.Value(0)).current;

  // use ref for second circle to start animation on this component
  const circleRef = useRef();
  const inputRef = useRef();

  // Size of circle
  const halfCircle = radius + strokeWidth;
  const circleCircumference = 2 * Math.PI * radius;

  // Define styling of animation
  const animation = (toValue) => {
    const animationReturn = Animated.timing(animatedValue, {
      toValue,
      duration: time * 1000,
      easing: Easing.linear,
      useNativeDriver: true,
    }).start();
  };

  // Animated circle when all is loaded with useEffect
  useEffect(() => {
    animation(time);

    // Listen animated value changes
    animatedValue.addListener((v) => {
      // Circle animation
      if (circleRef?.current) {
        // max time on one minute scale
        const maxTime = time === 0 ? (60 * v.value) / 1 : (60 * v.value) / time;

        // Calculate new size of strokeDashOffset
        const strokeDashoffset =
          circleCircumference - (circleCircumference * maxTime) / 60;

        // Update current native props of circle (strokeDashOffset)
        circleRef.current.setNativeProps({
          strokeDashoffset,
        });
      }

      // Text animation
      if (inputRef?.current) {
        let minutes = Math.floor(remainingTime / 60);
        let seconds = remainingTime % 60;

        if (minutes < 10) {
          minutes = `0${minutes}`;
        }
        if (seconds < 10) {
          seconds = `0${seconds}`;
        }

        inputRef.current.setNativeProps({
          text: `${minutes}:${seconds}`,
        });
      }

      return () => {
        animatedValue.removeAllListeners();
      };
    });
  }, [time]);

  return (
    <View>
      <Svg
        width={radius * 2}
        height={radius * 2}
        viewBox={`0 0 ${halfCircle * 2} ${halfCircle * 2}`}
      >
        <G rotation='-90' origin={`${halfCircle}, ${halfCircle}`}>
          <Circle
            cx='50%'
            cy='50%'
            r={radius}
            fill='transparent'
            stroke={color}
            strokeWidth={strokeWidth}
            strokeOpacity={0.2}
          />
          <AnimatedCircle
            ref={circleRef}
            cx='50%'
            cy='50%'
            r={radius}
            fill='transparent'
            stroke={color}
            strokeWidth={strokeWidth}
            strokeOpacity={1}
            strokeDasharray={circleCircumference}
            strokeDashoffset={circleCircumference}
            strokeLinecap='round'
          />
        </G>
      </Svg>
      <AnimatedInput
        ref={inputRef}
        underlineColorAndroid='transparent'
        defaultValue={'00:00'}
        keyboardType={'twitter'}
        editable={false}
        style={[
          StyleSheet.absoluteFillObject,
          { fontSize: radius / 2, color: color },
          styles.text,
        ]}
      />
    </View>
  );
};
