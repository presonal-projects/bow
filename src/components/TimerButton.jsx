import React from 'react';
import { Text, TouchableOpacity, StyleSheet } from 'react-native';

import colors from '../constants/colors';

const styles = StyleSheet.create({
  button: {
    width: 65,
    height: 65,
    borderRadius: 50,
    backgroundColor: colors.colorCircleThree,
    justifyContent: 'center',
    alignItems: 'center',
  },
  title: {
    color: colors.colorWhite,
    fontFamily: 'Montserrat_700Bold',
    fontSize: 16,
  },
});

export const TimerButton = (props) => {
  return (
    <TouchableOpacity style={styles.button} onPress={props.action}>
      <Text style={styles.title}>{props.title}</Text>
    </TouchableOpacity>
  );
};

export default TimerButton;
