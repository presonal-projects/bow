import React from 'react';
import { View, Text, TouchableOpacity, StyleSheet } from 'react-native';

const styles = StyleSheet.create({
  currency: {
    flex: 1,
  },
  currencyTitle: {
    color: '#FAFAFA',
    fontSize: 40,
    fontFamily: 'Montserrat_700Bold',
    marginLeft: 20,
    marginBottom: 0,
  },
  currencyText: {
    color: '#FAFAFA',
    fontSize: 30,
    fontFamily: 'Montserrat_500Medium',
    marginBottom: 30,
    marginLeft: 50,
  },
  buttonCurrency: {
    flex: 1,
    justifyContent: 'center',
  },
});

const CurrencyText = ({ currency, value, openModal }) => {
  return (
    <View style={styles.currency}>
      <TouchableOpacity style={styles.buttonCurrency} onPress={openModal}>
        <Text style={styles.currencyTitle}>{currency}</Text>
      </TouchableOpacity>
      <Text style={styles.currencyText}>{value}</Text>
    </View>
  );
};

export default CurrencyText;
