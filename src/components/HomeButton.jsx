import * as React from 'react';
import {
  TouchableHighlight,
  StyleSheet,
  View,
  Text,
  Image,
} from 'react-native';

import colors from '../constants/colors';

const styles = StyleSheet.create({
  container: {
    flex: 1,
    justifyContent: 'center',
    paddingHorizontal: 10,
    width: '45%',
    height: 120,
    marginVertical: 10,
    marginHorizontal: 5,
    borderRadius: 10,
    paddingVertical: 10,
  },
  button: {
    width: '100%',
    height: 120,
    padding: 10,
    elevation: 6,
    shadowColor: '#000',
    shadowOffset: { width: 0, height: 1 },
    shadowOpacity: 0.2,
    shadowRadius: 10,
    borderRadius: 10,
    backgroundColor: '#FAFAFA',
  },
  touchable: {
    borderRadius: 10,
  },
  btnTitle: {
    fontSize: 20,
    fontWeight: '600',
    fontFamily: 'Montserrat_500Medium',
    color: colors.colorGrey,
    marginLeft: 5,
  },
  btnIcon: {
    position: 'absolute',
    right: 10,
    bottom: 10,
  },
});

const ButtonHome = (props) => (
  <View style={styles.container}>
    <TouchableHighlight onPress={props.navigTo} style={styles.touchable}>
      <View style={styles.button}>
        <Text style={styles.btnTitle}>{props.title}</Text>
        <Image source={props.icon} style={styles.btnIcon} />
      </View>
    </TouchableHighlight>
  </View>
);

export default ButtonHome;
