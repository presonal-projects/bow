import * as React from 'react';
import { TouchableOpacity } from 'react-native';
import { NavigationContainer } from '@react-navigation/native';
import { createStackNavigator } from '@react-navigation/stack';
import { createDrawerNavigator } from '@react-navigation/drawer';
import { Entypo } from '@expo/vector-icons';

import Home from '../pages/Home';
import Random from '../pages/Random';
import Timer from '../pages/Timer';
import Wallet from '../pages/Wallet';
import Convertor from '../pages/Convertor';
import CurrencyList from '../pages/CurrencyList';
import colors from '../constants/colors';
import TimerRoller from '../pages/TimerRoller';
import Settings from '../pages/Settings';

const ModalStack = createStackNavigator();
const MainStack = createStackNavigator();
const MainDrawer = createDrawerNavigator();

const MainStackScreen = () => (
  <MainStack.Navigator initialRouteName='home'>
    <MainStack.Screen
      name='home'
      component={Home}
      options={{
        title: '',
        headerStyle: {
          backgroundColor: '#F22613',
          elevation: 0,
          shadowRadius: 0,
          shadowOffset: {
            height: 0,
          },
        },
        headerTintColor: '#fff',
        headerTitleStyle: {
          fontFamily: 'Montserrat_700Bold',
        },
        headerLeft: () => (
          <TouchableOpacity style={{ marginLeft: 20 }}>
            <Entypo name='menu' size={35} color='white' />
          </TouchableOpacity>
        ),
        headerRight: () => (
          <TouchableOpacity style={{ marginRight: 20 }}>
            <Entypo name='cog' size={30} color='white' />
          </TouchableOpacity>
        ),
      }}
    />
    <MainStack.Screen
      name='random'
      component={Random}
      options={{
        title: 'Random Number',
        headerStyle: {
          backgroundColor: '#F22613',
          shadowRadius: 0,
          shadowOffset: {
            height: 0,
          },
        },
        headerTintColor: '#fff',
        headerTitleStyle: {
          fontFamily: 'Montserrat_700Bold',
        },
      }}
    />
    <MainStack.Screen
      name='timer'
      component={Timer}
      options={{
        title: 'Timer',
        headerStyle: {
          backgroundColor: '#F2B705',
          shadowRadius: 0,
          shadowOffset: {
            height: 0,
          },
        },
        headerTintColor: '#fff',
        headerTitleStyle: {
          fontFamily: 'Montserrat_700Bold',
        },
      }}
    />
    <MainStack.Screen
      name='wallet'
      component={Wallet}
      options={{
        title: 'Wallet',
        headerStyle: {
          backgroundColor: '#F2E205',
          shadowRadius: 0,
          shadowOffset: {
            height: 0,
          },
        },
        headerTintColor: '#fff',
        headerTitleStyle: {
          fontFamily: 'Montserrat_700Bold',
        },
      }}
    />
    <MainStack.Screen
      name='convertor'
      component={Convertor}
      options={{
        title: 'Convertor',
        headerStyle: {
          backgroundColor: '#F26716',
          shadowRadius: 0,
          shadowOffset: {
            height: 0,
          },
        },
        headerTintColor: '#fff',
        headerTitleStyle: {
          fontFamily: 'Montserrat_700Bold',
        },
      }}
    />
  </MainStack.Navigator>
);

const ModalStackScreen = () => (
  <ModalStack.Navigator mode='modal'>
    <ModalStack.Screen
      name='Main'
      component={DrawerScreen}
      options={() => ({
        headerShown: false,
      })}
    />

    <ModalStack.Screen
      name='CurrencyList'
      component={CurrencyList}
      options={({ navigation, route }) => ({
        title: route.params && route.params.title,
        headerLeft: null,
        headerRight: () => (
          <TouchableOpacity onPress={() => navigation.pop()}>
            <Entypo
              name='cross'
              size={30}
              color={colors.colorWhite}
              style={{ marginRight: 10 }}
            />
          </TouchableOpacity>
        ),
        headerStyle: {
          backgroundColor: colors.colorOrange,
          elevation: 0,
          shadowRadius: 0,
          shadowOffset: {
            height: 0,
          },
        },
        headerTintColor: '#fff',
        headerTitleStyle: {
          fontFamily: 'Montserrat_700Bold',
        },
      })}
    />

    <ModalStack.Screen
      name='TimerRoller'
      component={TimerRoller}
      options={({ navigation }) => ({
        title: null,
        headerLeft: null,
        headerRight: () => (
          <TouchableOpacity onPress={() => navigation.pop()}>
            <Entypo
              name='chevron-down'
              size={30}
              style={{ marginRight: 10 }}
              color={colors.colorWhite}
            />
          </TouchableOpacity>
        ),
        headerStyle: {
          backgroundColor: colors.colorCircleThree,
          elevation: 0,
          shadowOffset: {
            height: 0,
          },
          shadowRadius: 0,
        },
      })}
    />
  </ModalStack.Navigator>
);

const DrawerScreen = () => (
  <MainDrawer.Navigator initialRouteName='home'>
    <MainDrawer.Screen
      name='Home'
      component={Home}
      options={() => ({
        headerShown: false,
      })}
    />
    <MainDrawer.Screen
      name='random'
      component={Random}
      options={{
        title: 'Random Number',
        headerStyle: {
          backgroundColor: '#F22613',
          shadowRadius: 0,
          shadowOffset: {
            height: 0,
          },
        },
        headerTintColor: '#fff',
        headerTitleStyle: {
          fontFamily: 'Montserrat_700Bold',
        },
      }}
    />
    <MainDrawer.Screen
      name='timer'
      component={Timer}
      options={{
        title: 'Timer',
        headerStyle: {
          backgroundColor: '#F2B705',
          shadowRadius: 0,
          shadowOffset: {
            height: 0,
          },
        },
        headerTintColor: '#fff',
        headerTitleStyle: {
          fontFamily: 'Montserrat_700Bold',
        },
      }}
    />
    <MainDrawer.Screen
      name='wallet'
      component={Wallet}
      options={{
        title: 'Wallet',
        headerStyle: {
          backgroundColor: '#F2E205',
          shadowRadius: 0,
          shadowOffset: {
            height: 0,
          },
        },
        headerTintColor: '#fff',
        headerTitleStyle: {
          fontFamily: 'Montserrat_700Bold',
        },
      }}
    />
    <MainDrawer.Screen
      name='convertor'
      component={Convertor}
      options={{
        title: 'Convertor',
        headerStyle: {
          backgroundColor: '#F26716',
          shadowRadius: 0,
          shadowOffset: {
            height: 0,
          },
        },
        headerTintColor: '#fff',
        headerTitleStyle: {
          fontFamily: 'Montserrat_700Bold',
        },
      }}
    />
    <MainDrawer.Screen
      name='settings'
      component={Settings}
      options={{
        title: 'Settings',
        headerStyle: {
          backgroundColor: '#000',
          shadowRadius: 0,
          shadowOffset: {
            height: 0,
          },
        },
        headerTintColor: '#fff',
        headerTitleStyle: {
          fontFamily: 'Montserrat_700Bold',
        },
      }}
    />
  </MainDrawer.Navigator>
);

export default () => (
  <NavigationContainer>
    <DrawerScreen />
  </NavigationContainer>
);
